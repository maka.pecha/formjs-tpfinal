import Form from './modules/form-by-steps/form.js';

class Main extends Form {

  constructor() {
    super();
    this.initChildSelector(this.onChangeChildSelector);
    console.log('Script init');
  }

  initChildSelector(onChangeChildSelector) {
    const $childSelector = $('#childGroupSelectUnder18');

    $childSelector.on('change', function () {
      onChangeChildSelector(this.value);
    });
  }

  onChangeChildSelector(evt) {
      let childId = 0;
      let labelId = 0;
      $('#toAddChild').html(null);
      for (let i = 1; i <= evt; i++) {
          childId = i;
          labelId = i;
          const mySecondDiv = $('<div class="form-row childrenData" name="ChildComponent-' + childId + '" id="ChildComponent-' + childId + '">\n' +
            '          <div class="col-md-1 mb-3">\n' +
            '            <label>Child</label>\n' +
            '            <div class="input-group-prepend">\n' +
            '              <label class="input-group-text" for="inputGroupSelect01">#' + labelId + '</label>\n' +
            '            </div>\n' +
            '          </div>\n' +
            '          <div class="col-md-4 mb-3">\n' +
            '            <label for="childName">First name</label>\n' +
            '            <input type="text" class="form-control" id="childName-' + childId + '" name="childName-' + childId + '" placeholder="First name" value="Mark Jr." required>\n' +
            '            <div class="valid-feedback">\n' +
            '              Looks good!\n' +
            '            </div>\n' +
            '          </div>\n' +
            '          <div class="col-md-4 mb-3">\n' +
            '            <label for="childLastName">Last name</label>\n' +
            '            <input type="text" class="form-control" id="childLastName-' + childId + '" name="childLastName-' + childId + '" placeholder="Last name" value="Otto" required>\n' +
            '            <div class="valid-feedback">\n' +
            '              Looks good!\n' +
            '            </div>\n' +
            '          </div>\n' +
            '          <div class="col-md-3 mb-3">\n' +
            '            <label for="childBirthdate">Birthdate</label>\n' +
            '            <input type="date" class="form-control" id="childBirthdate-' + childId + '" name="childBirthdate-' + childId + '" placeholder="mm/dd/yyyy" value="" required>\n' +
            '            <div class="valid-feedback">\n' +
            '              Looks good!\n' +
            '            </div>\n' +
            '          </div>\n' +
            '        </div>');
          $('#toAddChild').append(mySecondDiv);
      }
  }

  // En el paso 2 al seleccionar los hijos menores de 18 años debe mostrar
  // los datos a completar de los hijos según el número de los mismos, por
  // lo tanto se deberia copiar la fila Child #1 y generar para los demás,
  // si se cambia nuevamente el número de hijos éste debe cambiar también por
  // lo que si selecciona None o al inicio no debe mostrar dicha fila Child #1

}

let main = new Main();