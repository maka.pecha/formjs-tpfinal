import FormEvents from './form_events.js';
import FormValidation from './form_validation.js';

export default class Form {

    constructor() {
        this.formEvents = new FormEvents();
        this.formValidation = new FormValidation();
        this.initSendFormAction();
    }

    initSendFormAction() {
        this.formEvents.initSendFormEvent(this.sendForm.bind(this));
        this.formEvents.initPrevNextButtons(this.validations.bind(this));
    }

    validations(event){
      const userName = document.getElementById('validationCustomUsername').value;
      const firstName = document.getElementById('validationCustom01').value;
      const lastName = document.getElementById('validationCustom02').value;
      const regexUserName = /^[a-zA-Z.]*$/;
      const regexName = /^[a-zA-Z ]*$/;

      if (!regexName.test(firstName) || firstName === "") {
        alert("Wrong first name! Only characters are accepted and as the only separator the space is accepted (for example: \"Mark Ryan\")");
      } else if (!regexName.test(lastName) || lastName === "") {
        alert("Wrong last name! Only characters are accepted and as the only separator the space is accepted (for example: \"Otto Smith\")");
      } else if(!regexUserName.test(userName) || userName === "") {
        alert("Wrong Username! Only characters are accepted and as the only separator the point is accepted (for example: \"my.username\")");
      } else { //nextStep
        const $formStep = this.formEvents.action(event);
        const $nextStep = $(this.formEvents.goToStep($formStep[0].classList[1]));
        $nextStep.removeClass('d-none');
      }
    }

    sendForm() {
        console.log('Sending form data...');

        // Escribir aquí el código para enviar todos los datos del formulario.
        // enviar a https://reqres.in/api/users (Esta web sirve para realizar pruebas REST API, por lo tanto NO ENVIAR DATOS REALES)
        // la API retorna un JSON con el id del usuario creado y la info guardada.
        // Para mas info ver en https://reqres.in

        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/javascript");

        const formdata = {};
        $(".form").serializeArray().forEach( ({name, value}) => {
          formdata[name] = value;
        });
        console.log(formdata, 'my array');

        const requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: formdata,
          redirect: 'follow'
        };

        fetch("https://reqres.in/api/users", requestOptions)
          .then(response => response.text())
          .then(result => alert(result))
          .catch(error => console.log('error', error));
    }

}
