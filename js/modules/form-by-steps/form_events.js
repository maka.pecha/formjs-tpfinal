export default class Form_Events {
    constructor() {
        this.initPrevNextButtons();
    }

    initPrevNextButtons(callback) {
        const $prevButton = $('.js-previous');
        const $nextButton = $('.js-next');
        
        $prevButton.click(this.previousAction.bind(this));
        $nextButton.click(callback); //para que el this apunte a la funcion de todo el form en esta clase
    }

    initSendFormEvent(callback) { // trae sendForm
        const $sendForm = $('.js-sendForm');
        $sendForm.click(callback);
    }

    goToStep(step, direction = 'next') {
        // 2) Simplifica esta función para que sean menos líneas.
        const currentStep = +step.replace(/^step\-/, '');
        let goToStep = '.step-';

        const nextStep = ('next' === direction) ? currentStep + 1 : currentStep - 1;
        goToStep += nextStep;

        this.progressBar(Math.round(nextStep*33.33));
        return goToStep;
    }

    action(evt){
        const $current = $(evt.currentTarget);
        const $formStep = $current.parents('.form-step');

        $formStep.addClass('d-none');
        return $formStep
    }

    previousAction(evt) {
        const $formStep = this.action(evt);

        const $prevStep = $(this.goToStep($formStep[0].classList[1], 'prev'));
        $prevStep.removeClass('d-none');
    }

    // nextAction(evt) {
    //     // 3) ¿Se puede evitar repetir mismas líneas que en previousAction?
    //     const $formStep = this.action(evt);
    //
    //     let $nextStep = $(this.goToStep($formStep[0].classList[1]));
    //     $nextStep.removeClass('d-none');
    // }

    progressBar(percent) {
        // 1) Escribir aqui como sería la lógica para incrementar la barra de porcentaje.
        const $progressBar = $('.progress-bar');

        $progressBar
          .css('width', percent + '%')
          .html(percent + '%')
    }
    
}
