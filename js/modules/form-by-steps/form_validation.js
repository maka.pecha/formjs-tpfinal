export default class Form_Validation {

    constructor() {
        this.formSubmit();
    }

    formSubmit() {
        'use strict';
        window.addEventListener('load', function() {
            // Obtener todos los formularios a los que hay que aplicar estilos personalizados de validación Bootstrap
            const forms = document.getElementsByClassName('needs-validation');

            // Bucle sobre ellas y evitar la submision
            const validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    $('.js-sendForm').prop('disabled', !form.checkValidity());
                    event.preventDefault();
                    event.stopPropagation();
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    }

    // Crear métodos para validar aqui.
    // Mirar documentación de Bootstrap si es necesario:
    // https://getbootstrap.com/docs/4.0/components/forms/#validation

}